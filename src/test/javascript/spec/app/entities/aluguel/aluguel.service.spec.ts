/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { AluguelService } from 'app/entities/aluguel/aluguel.service';
import { IAluguel, Aluguel } from 'app/shared/model/aluguel.model';

describe('Service Tests', () => {
    describe('Aluguel Service', () => {
        let injector: TestBed;
        let service: AluguelService;
        let httpMock: HttpTestingController;
        let elemDefault: IAluguel;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(AluguelService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new Aluguel(0, currentDate, currentDate, currentDate, 0, 0);
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        data_ini_aluguel: currentDate.format(DATE_FORMAT),
                        data_prev_devolucao: currentDate.format(DATE_FORMAT),
                        data_real_devolucao: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a Aluguel', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        data_ini_aluguel: currentDate.format(DATE_FORMAT),
                        data_prev_devolucao: currentDate.format(DATE_FORMAT),
                        data_real_devolucao: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        data_ini_aluguel: currentDate,
                        data_prev_devolucao: currentDate,
                        data_real_devolucao: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new Aluguel(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a Aluguel', async () => {
                const returnedFromService = Object.assign(
                    {
                        data_ini_aluguel: currentDate.format(DATE_FORMAT),
                        data_prev_devolucao: currentDate.format(DATE_FORMAT),
                        data_real_devolucao: currentDate.format(DATE_FORMAT),
                        quantidade_dias: 1,
                        valor_aluguel: 1
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        data_ini_aluguel: currentDate,
                        data_prev_devolucao: currentDate,
                        data_real_devolucao: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of Aluguel', async () => {
                const returnedFromService = Object.assign(
                    {
                        data_ini_aluguel: currentDate.format(DATE_FORMAT),
                        data_prev_devolucao: currentDate.format(DATE_FORMAT),
                        data_real_devolucao: currentDate.format(DATE_FORMAT),
                        quantidade_dias: 1,
                        valor_aluguel: 1
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        data_ini_aluguel: currentDate,
                        data_prev_devolucao: currentDate,
                        data_real_devolucao: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a Aluguel', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
