package br.com.aluguel.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Reserva.
 */
@Entity
@Table(name = "reserva")
public class Reserva implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "data_ini_reserva", nullable = false)
    private LocalDate data_ini_reserva;

    @NotNull
    @Column(name = "data_fim_reserva", nullable = false)
    private LocalDate data_fim_reserva;

    @Column(name = "status")
    private String status;

    @OneToOne(optional = false)    
    @JoinColumn()
    private Cliente cliente_id;

    @OneToOne(optional = false)    
    @JoinColumn()
    private Veiculo veiculo_id;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getData_ini_reserva() {
        return data_ini_reserva;
    }

    public Reserva data_ini_reserva(LocalDate data_ini_reserva) {
        this.data_ini_reserva = data_ini_reserva;
        return this;
    }

    public void setData_ini_reserva(LocalDate data_ini_reserva) {
        this.data_ini_reserva = data_ini_reserva;
    }

    public LocalDate getData_fim_reserva() {
        return data_fim_reserva;
    }

    public Reserva data_fim_reserva(LocalDate data_fim_reserva) {
        this.data_fim_reserva = data_fim_reserva;
        return this;
    }

    public void setData_fim_reserva(LocalDate data_fim_reserva) {
        this.data_fim_reserva = data_fim_reserva;
    }

    public String getStatus() {
        return status;
    }

    public Reserva status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Cliente getCliente_id() {
        return cliente_id;
    }

    public Reserva cliente_id(Cliente cliente) {
        this.cliente_id = cliente;
        return this;
    }

    public void setCliente_id(Cliente cliente) {
        this.cliente_id = cliente;
    }

    public Veiculo getVeiculo_id() {
        return veiculo_id;
    }

    public Reserva veiculo_id(Veiculo veiculo) {
        this.veiculo_id = veiculo;
        return this;
    }

    public void setVeiculo_id(Veiculo veiculo) {
        this.veiculo_id = veiculo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reserva reserva = (Reserva) o;
        if (reserva.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reserva.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Reserva{" +
            "id=" + getId() +
            ", data_ini_reserva='" + getData_ini_reserva() + "'" +
            ", data_fim_reserva='" + getData_fim_reserva() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
