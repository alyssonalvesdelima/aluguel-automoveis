package br.com.aluguel.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Aluguel.
 */
@Entity
@Table(name = "aluguel")
public class Aluguel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "data_ini_aluguel", nullable = false)
    private LocalDate data_ini_aluguel;

    @NotNull
    @Column(name = "data_prev_devolucao", nullable = false)
    private LocalDate data_prev_devolucao;

    @Column(name = "data_real_devolucao")
    private LocalDate data_real_devolucao;

    @NotNull
    @Min(value = 1)
    @Column(name = "quantidade_dias", nullable = false)
    private Integer quantidade_dias;

    @Column(name = "valor_aluguel")
    private Double valor_aluguel;

    @OneToOne(optional = false)    
    @NotNull
    @JoinColumn(unique = true)
    private Cliente cliente_id;

    @OneToOne  @NotNull
    @JoinColumn(unique = true)
    private Veiculo veiculo_id;

    @OneToOne @NotNull
    @JoinColumn(unique = true)
    private Reserva reserva_id;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getData_ini_aluguel() {
        return data_ini_aluguel;
    }

    public Aluguel data_ini_aluguel(LocalDate data_ini_aluguel) {
        this.data_ini_aluguel = data_ini_aluguel;
        return this;
    }

    public void setData_ini_aluguel(LocalDate data_ini_aluguel) {
        this.data_ini_aluguel = data_ini_aluguel;
    }

    public LocalDate getData_prev_devolucao() {
        return data_prev_devolucao;
    }

    public Aluguel data_prev_devolucao(LocalDate data_prev_devolucao) {
        this.data_prev_devolucao = data_prev_devolucao;
        return this;
    }

    public void setData_prev_devolucao(LocalDate data_prev_devolucao) {
        this.data_prev_devolucao = data_prev_devolucao;
    }

    public LocalDate getData_real_devolucao() {
        return data_real_devolucao;
    }

    public Aluguel data_real_devolucao(LocalDate data_real_devolucao) {
        this.data_real_devolucao = data_real_devolucao;
        return this;
    }

    public void setData_real_devolucao(LocalDate data_real_devolucao) {
        this.data_real_devolucao = data_real_devolucao;
    }

    public Integer getQuantidade_dias() {
        return quantidade_dias;
    }

    public Aluguel quantidade_dias(Integer quantidade_dias) {
        this.quantidade_dias = quantidade_dias;
        return this;
    }

    public void setQuantidade_dias(Integer quantidade_dias) {
        this.quantidade_dias = quantidade_dias;
    }

    public Double getValor_aluguel() {
        return valor_aluguel;
    }

    public Aluguel valor_aluguel(Double valor_aluguel) {
        this.valor_aluguel = valor_aluguel;
        return this;
    }

    public void setValor_aluguel(Double valor_aluguel) {
        this.valor_aluguel = valor_aluguel;
    }

    public Cliente getCliente_id() {
        return cliente_id;
    }

    public Aluguel cliente_id(Cliente cliente) {
        this.cliente_id = cliente;
        return this;
    }

    public void setCliente_id(Cliente cliente) {
        this.cliente_id = cliente;
    }

    public Veiculo getVeiculo_id() {
        return veiculo_id;
    }

    public Aluguel veiculo_id(Veiculo veiculo) {
        this.veiculo_id = veiculo;
        return this;
    }

    public void setVeiculo_id(Veiculo veiculo) {
        this.veiculo_id = veiculo;
    }

    public Reserva getReserva_id() {
        return reserva_id;
    }

    public Aluguel reserva_id(Reserva reserva) {
        this.reserva_id = reserva;
        return this;
    }

    public void setReserva_id(Reserva reserva) {
        this.reserva_id = reserva;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Aluguel aluguel = (Aluguel) o;
        if (aluguel.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), aluguel.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Aluguel{" +
            "id=" + getId() +
            ", data_ini_aluguel='" + getData_ini_aluguel() + "'" +
            ", data_prev_devolucao='" + getData_prev_devolucao() + "'" +
            ", data_real_devolucao='" + getData_real_devolucao() + "'" +
            ", quantidade_dias=" + getQuantidade_dias() +
            ", valor_aluguel=" + getValor_aluguel() +
            "}";
    }
}
