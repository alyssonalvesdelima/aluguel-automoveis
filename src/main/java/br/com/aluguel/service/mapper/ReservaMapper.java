package br.com.aluguel.service.mapper;

import br.com.aluguel.domain.*;
import br.com.aluguel.service.dto.ReservaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Reserva and its DTO ReservaDTO.
 */
@Mapper(componentModel = "spring", uses = {ClienteMapper.class, VeiculoMapper.class})
public interface ReservaMapper extends EntityMapper<ReservaDTO, Reserva> {

    @Mapping(source = "cliente_id.id", target = "cliente_idId")
    @Mapping(source = "cliente_id.cliente_id", target = "cliente_idCliente_id")
    @Mapping(source = "veiculo_id.id", target = "veiculo_idId")
    @Mapping(source = "veiculo_id.veiculo_id", target = "veiculo_idVeiculo_id")
    ReservaDTO toDto(Reserva reserva);

    @Mapping(source = "cliente_idId", target = "cliente_id")
    @Mapping(source = "veiculo_idId", target = "veiculo_id")
    Reserva toEntity(ReservaDTO reservaDTO);

    default Reserva fromId(Long id) {
        if (id == null) {
            return null;
        }
        Reserva reserva = new Reserva();
        reserva.setId(id);
        return reserva;
    }
}
