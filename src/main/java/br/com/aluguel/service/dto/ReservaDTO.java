package br.com.aluguel.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;

import br.com.aluguel.domain.Cliente;
import br.com.aluguel.domain.Veiculo;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Reserva entity.
 */
public class ReservaDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate data_ini_reserva;

    @NotNull
    private LocalDate data_fim_reserva;

    private String status;

    private Long cliente_id;

    private Long veiculo_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getData_ini_reserva() {
        return data_ini_reserva;
    }

    public void setData_ini_reserva(LocalDate data_ini_reserva) {
        this.data_ini_reserva = data_ini_reserva;
    }

    public LocalDate getData_fim_reserva() {
        return data_fim_reserva;
    }

    public void setData_fim_reserva(LocalDate data_fim_reserva) {
        this.data_fim_reserva = data_fim_reserva;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Long clienteId) {
        this.cliente_id = clienteId;
    }

    public Long getVeiculo_id() {
        return veiculo_id;
    }

    public void setVeiculo_idId(Long veiculoId) {
        this.veiculo_id = veiculoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReservaDTO reservaDTO = (ReservaDTO) o;
        if (reservaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reservaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReservaDTO{" +
            "id=" + getId() +
            ", data_ini_reserva='" + getData_ini_reserva() + "'" +
            ", data_fim_reserva='" + getData_fim_reserva() + "'" +
            ", status='" + getStatus() + "'" +
            ", cliente_id=" + getCliente_id() +
            ", veiculo_id=" + getVeiculo_id() +
            "}";
    }
}
