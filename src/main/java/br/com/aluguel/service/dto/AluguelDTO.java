package br.com.aluguel.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Aluguel entity.
 */
public class AluguelDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate data_ini_aluguel;

    @NotNull
    private LocalDate data_prev_devolucao;

    private LocalDate data_real_devolucao;

    @NotNull
    @Min(value = 1)
    private Integer quantidade_dias;

    private Double valor_aluguel;

    private Long cliente_idId;

    private String cliente_idCliente_id;

    private Long veiculo_idId;

    private String veiculo_idVeiculo_id;

    private Long reserva_idId;

    private String reserva_idReserva_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getData_ini_aluguel() {
        return data_ini_aluguel;
    }

    public void setData_ini_aluguel(LocalDate data_ini_aluguel) {
        this.data_ini_aluguel = data_ini_aluguel;
    }

    public LocalDate getData_prev_devolucao() {
        return data_prev_devolucao;
    }

    public void setData_prev_devolucao(LocalDate data_prev_devolucao) {
        this.data_prev_devolucao = data_prev_devolucao;
    }

    public LocalDate getData_real_devolucao() {
        return data_real_devolucao;
    }

    public void setData_real_devolucao(LocalDate data_real_devolucao) {
        this.data_real_devolucao = data_real_devolucao;
    }

    public Integer getQuantidade_dias() {
        return quantidade_dias;
    }

    public void setQuantidade_dias(Integer quantidade_dias) {
        this.quantidade_dias = quantidade_dias;
    }

    public Double getValor_aluguel() {
        return valor_aluguel;
    }

    public void setValor_aluguel(Double valor_aluguel) {
        this.valor_aluguel = valor_aluguel;
    }

    public Long getCliente_idId() {
        return cliente_idId;
    }

    public void setCliente_idId(Long clienteId) {
        this.cliente_idId = clienteId;
    }

    public String getCliente_idCliente_id() {
        return cliente_idCliente_id;
    }

    public void setCliente_idCliente_id(String clienteCliente_id) {
        this.cliente_idCliente_id = clienteCliente_id;
    }

    public Long getVeiculo_idId() {
        return veiculo_idId;
    }

    public void setVeiculo_idId(Long veiculoId) {
        this.veiculo_idId = veiculoId;
    }

    public String getVeiculo_idVeiculo_id() {
        return veiculo_idVeiculo_id;
    }

    public void setVeiculo_idVeiculo_id(String veiculoVeiculo_id) {
        this.veiculo_idVeiculo_id = veiculoVeiculo_id;
    }

    public Long getReserva_idId() {
        return reserva_idId;
    }

    public void setReserva_idId(Long reservaId) {
        this.reserva_idId = reservaId;
    }

    public String getReserva_idReserva_id() {
        return reserva_idReserva_id;
    }

    public void setReserva_idReserva_id(String reservaReserva_id) {
        this.reserva_idReserva_id = reservaReserva_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AluguelDTO aluguelDTO = (AluguelDTO) o;
        if (aluguelDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), aluguelDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AluguelDTO{" +
            "id=" + getId() +
            ", data_ini_aluguel='" + getData_ini_aluguel() + "'" +
            ", data_prev_devolucao='" + getData_prev_devolucao() + "'" +
            ", data_real_devolucao='" + getData_real_devolucao() + "'" +
            ", quantidade_dias=" + getQuantidade_dias() +
            ", valor_aluguel=" + getValor_aluguel() +
            ", cliente_id=" + getCliente_idId() +
            ", cliente_id='" + getCliente_idCliente_id() + "'" +
            ", veiculo_id=" + getVeiculo_idId() +
            ", veiculo_id='" + getVeiculo_idVeiculo_id() + "'" +
            ", reserva_id=" + getReserva_idId() +
            ", reserva_id='" + getReserva_idReserva_id() + "'" +
            "}";
    }
}
