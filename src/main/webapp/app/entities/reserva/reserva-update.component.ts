import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';

import { IReserva } from 'app/shared/model/reserva.model';
import { ReservaService } from './reserva.service';
import { ICliente } from 'app/shared/model/cliente.model';
import { ClienteService } from 'app/entities/cliente';
import { IVeiculo } from 'app/shared/model/veiculo.model';
import { VeiculoService } from 'app/entities/veiculo';

@Component({
    selector: 'jhi-reserva-update',
    templateUrl: './reserva-update.component.html'
})
export class ReservaUpdateComponent implements OnInit {
    reserva: IReserva;
    isSaving: boolean;

    cliente_ids: ICliente[];

    veiculo_ids: IVeiculo[];
    data_ini_reservaDp: any;
    data_fim_reservaDp: any;

    constructor(
        private jhiAlertService: JhiAlertService,
        private reservaService: ReservaService,
        private clienteService: ClienteService,
        private veiculoService: VeiculoService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ reserva }) => {
            this.reserva = reserva;
        });
        this.clienteService.query({ filter: 'cliente_id-is-null' }).subscribe(
            (res: HttpResponse<ICliente[]>) => {
                if (!this.reserva.cliente_id) {
                    this.cliente_ids = res.body;

                    console.log("+++++++++++++++" + this.cliente_ids[0].nome);

                } else {
                    this.clienteService.find(this.reserva.cliente_id).subscribe(
                        (subRes: HttpResponse<ICliente>) => {
                            this.cliente_ids = [subRes.body].concat(res.body);
                            console.log("==========" + this.cliente_ids);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.veiculoService.query({ filter: 'veiculo_id-is-null' }).subscribe(
            (res: HttpResponse<IVeiculo[]>) => {
                if (!this.reserva.veiculo_id) {
                    this.veiculo_ids = res.body;
                } else {
                    this.veiculoService.find(this.reserva.veiculo_id).subscribe(
                        (subRes: HttpResponse<IVeiculo>) => {
                            this.veiculo_ids = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.reserva.id !== undefined) {
            this.subscribeToSaveResponse(this.reservaService.update(this.reserva));
        } else {
            this.subscribeToSaveResponse(this.reservaService.create(this.reserva));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IReserva>>) {
        result.subscribe((res: HttpResponse<IReserva>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackClienteById(index: number, item: ICliente) {
        return item.id;
    }

    trackVeiculoById(index: number, item: IVeiculo) {
        return item.id;
    }
}
