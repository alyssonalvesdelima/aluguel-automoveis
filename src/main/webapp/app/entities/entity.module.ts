import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AluguelAutomoveisClienteModule } from './cliente/cliente.module';
import { AluguelAutomoveisVeiculoModule } from './veiculo/veiculo.module';
import { AluguelAutomoveisReservaModule } from './reserva/reserva.module';
import { AluguelAutomoveisAluguelModule } from './aluguel/aluguel.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        AluguelAutomoveisClienteModule,
        AluguelAutomoveisVeiculoModule,
        AluguelAutomoveisReservaModule,
        AluguelAutomoveisAluguelModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AluguelAutomoveisEntityModule {}
