import { Moment } from 'moment';

export interface IReserva {
    id?: number;
    data_ini_reserva?: Moment;
    data_fim_reserva?: Moment;
    status?: string;
    cliente_id?: number;
    veiculo_id?: number;
}

export class Reserva implements IReserva {
    constructor(
        public id?: number,
        public data_ini_reserva?: Moment,
        public data_fim_reserva?: Moment,
        public status?: string,
        public cliente_id?: number,
        public veiculo_id?: number
    ) {}
}
