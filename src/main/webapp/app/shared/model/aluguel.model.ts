import { Moment } from 'moment';

export interface IAluguel {
    id?: number;
    data_ini_aluguel?: Moment;
    data_prev_devolucao?: Moment;
    data_real_devolucao?: Moment;
    quantidade_dias?: number;
    valor_aluguel?: number;
    cliente_idCliente_id?: string;
    cliente_idId?: number;
    veiculo_idVeiculo_id?: string;
    veiculo_idId?: number;
    reserva_idReserva_id?: string;
    reserva_idId?: number;
}

export class Aluguel implements IAluguel {
    constructor(
        public id?: number,
        public data_ini_aluguel?: Moment,
        public data_prev_devolucao?: Moment,
        public data_real_devolucao?: Moment,
        public quantidade_dias?: number,
        public valor_aluguel?: number,
        public cliente_idCliente_id?: string,
        public cliente_idId?: number,
        public veiculo_idVeiculo_id?: string,
        public veiculo_idId?: number,
        public reserva_idReserva_id?: string,
        public reserva_idId?: number
    ) {}
}
